from abc import ABC, abstractclassmethod

class Person(ABC):
	@abstractclassmethod

	def getFullName(self):
		pass

	def addRequest(self):
		pass

	def checkRequest(self):
		pass

	def addUser(self):
		pass

class Employee(Person):
	def __init__(self, firstname, lastname, email, dept):
		self._firstName = firstname
		self._lastName = lastname
		self._email = email
		self._department = dept

	# Getters
	def get_firstname(self):
		return self._firstName

	def get_lastname(self):
		return self._lastName

	def get_email(self):
		return self._email

	def get_department(self):
		return self._department

	def getFullName(self):
		return f"{self._firstName} {self._lastName}"

	# Setters
	def set_firstname(self, firstname):
		self._firstName = firstname

	def set_lastname(self, lastname):
		self._lastName = lastname

	def set_email(self, email):
		self._email = email

	def set_department(self, department):
		self._department = department
	


	def addRequest(self):
		return "Request has been added"

	def getFullName(self):
		return f"{self._firstName} {self._lastName}"

	def login(self):
		return f"{self._email} has logged in"

	def logout(self):
		return f"{self._email} has logged out"

class TeamLead(Person):
	def __init__(self, firstname, lastname, email, dept):
		self._firstName = firstname
		self._lastName = lastname
		self._email = email
		self._department = dept
		self._members = []

	# Getters
	def get_firstname(self):
		return self._firstName

	def get_lastname(self):
		return self._lastName

	def get_email(self):
		return self._email

	def get_department(self):
		return self._department

	def get_members(self):
		return self._members

	def getFullName(self):
		return f"{self._firstName} {self._lastName}"



	# Setters
	def set_firstname(self, firstname):
		self._firstName = firstname

	def set_lastname(self, lastname):
		self._lastName = lastname

	def set_email(self, email):
		self._email = email

	def set_department(self, department):
		self._department = department


	def addMember(self, employee):
		self._members.append(employee)

	def login(self):
		return f"{self._email} has logged in"

	def logout(self):
		return f"{self._email} has logged out"

class Admin(Person):
	def __init__(self, firstname, lastname, email, dept):
		self._firstName = firstname
		self._lastName = lastname
		self._email = email
		self._department = dept

	# Getters
	def get_firstname(self):
		return self._firstName

	def get_lastname(self):
		return self._lastName

	def get_email(self):
		return self._email

	def get_department(self):
		return self._department

	def getFullName(self):
		return f"{self._firstName} {self._lastName}"

	# Setters
	def set_firstname(self, firstname):
		self._firstName = firstname

	def set_lastname(self, lastname):
		self._lastName = lastname

	def set_email(self, email):
		self._email = email

	def set_department(self, department):
		self._department = department


	def addUser(self):
		return "User has been added"

	def login(self):
		return f"{self._email} has logged in"

	def logout(self):
		return f"{self._email} has logged out"

class Request():
	def __init__(self, name, requester, dateRequested):
		self._name = name
		self._requester = requester
		self._dateRequested = dateRequested
		self._status = "Pending"

	# Getters

	def get_name(self):
		return self._name

	def get_requester(self):
		return self._requester

	def get_status(self):
		return self._status

	def get_date(self):
		return self._dateRequested

	# Setters
	def set_name(self, name):
		self._name = name

	def set_requester(self, requester):
		self._requester = requester

	def set_status(self, status):
		self._status = status

	def set_date(self, dateRequested):
		self._dateRequested = dateRequested


	def updateRequest(self, newstatus):
		self._status = newstatus

	def closeRequest(self):
		self._status = "Closed"
		return f"{self._name} has been closed"

	def cancelRequest(self):
		self._status = "Cancelled"
		return f"{self._name} has been cancelled"


employee1 = Employee ("John", "Doe", "djohn@mail.com", "Marketing")
employee2 = Employee ("Jane", "Smith", "sjane@mail.com", "Marketing")
employee3 = Employee ("Robert", "Patterson", "probert@mail.com", "Sales")
employee4 = Employee ("Brandon", "Smith", "sbrandon@mail.com", "Sales")
admin1 = Admin ("Monika", "Justin", "jmonika@mail.com", "Marketing")
teamlead1 = TeamLead ("Michael", "Specter", "smichael@mail.com", "Sales")
req1 = Request ("New hire orientation", teamlead1, "27-Jul-2021")
req2 = Request ("Laptop repair", employee1, "1-Jul-2021")


assert employee1.getFullName() == "John Doe", "Full name should be John Doe"
assert admin1.getFullName() == "Monika Justin", "Full name should be Monika Justin"
assert teamlead1.getFullName() == "Michael Specter", "Full name should be Michael Specter"
assert employee2.login() == "sjane@mail.com has logged in"
assert employee2.addRequest() == "Request has been added"
assert employee2.logout() == "sjane@mail.com has logged out"

teamlead1.addMember(employee3)
teamlead1.addMember(employee4)

for indiv_emp in teamlead1.get_members():
	print(indiv_emp.getFullName())

assert admin1.addUser() == "User has been added"

req2.set_status('closed')
print(req2.closeRequest())