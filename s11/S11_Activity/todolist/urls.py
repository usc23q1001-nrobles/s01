from django.urls import path
from . import views


# Syntax
# path(route, view, name)
app_name = 'todolist'

urlpatterns = [
	# localhost:8000/todolist/
	path('', views.index, name='index'),

	# localhost:8000/todolist/<todoitem_id>
	path('<int:todoitem_id>/', views.todoitem, name='viewtodoitem'),

	# localhost:8000/todolist/events/<event_id>
	path('event/<int:event_id>/', views.event, name='viewevent'),

	# /todolist/register
	path('register', views.register, name='register'),

	# /todolist/change_password
	# path('change_password', views.change_password, name='change_password'),

	# /todolist/login
	path('login', views.login_view, name='login'),

	# /todolist/logout
	path('logout', views.logout_view, name='logout'),

	# /todolist/add_task
	path('add_task', views.add_task, name="add_task"),

	# /todolist/add_event
	path('add_event', views.add_event, name="add_event"),

	# /todolist/update_profile
	path('update_profile', views.update_profile, name="update_profile"),

	# /todolist/<todoitem_id>/edit
	path('<int:todoitem_id>/edit', views.update_task, name="update_task"),

	# /todolist/<todoitem_id>/delete
	path('<int:todoitem_id>/delete', views.delete_task, name="delete_task"),

	

]