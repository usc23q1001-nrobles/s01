from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse
from django.template import loader

from django.contrib.auth.models import User	# User Object
from django.contrib.auth import authenticate, hashers, login, logout
from django.forms.models import model_to_dict
from django.utils import timezone

from .models import ToDoItem, Event
from .forms import LoginForm, RegisterForm, AddTaskForm, UpdateTaskForm, AddEventForm, UpdateProfileForm

def index(request):
	todoitem_list = ToDoItem.objects.filter(user_id=request.user.id)
	event_list = Event.objects.filter(user_id=request.user.id)

	context = { 
		'todoitem_list' : todoitem_list,
		'event_list' : event_list,
		'user' : request.user 
	}
	return render(request, "todolist/index.html", context)

def todoitem(request, todoitem_id):
	todoitem = get_object_or_404(ToDoItem, pk=todoitem_id)
	return render(request, "todolist/todoitem.html", model_to_dict(todoitem))

def event(request, event_id):
	event = get_object_or_404(Event, pk=event_id)
	return render(request, "todolist/event.html", model_to_dict(event))

def register(request):
	context = {}
	if request.method == 'POST':
		form = RegisterForm(request.POST)
		if form.is_valid() == False:	
			form = RegisterForm()
		else:
			duplicates = User.objects.filter(username=form.cleaned_data['username'])

			if not duplicates:
				user = User()
				user.username = form.cleaned_data['username']
				user.first_name = form.cleaned_data['first_name']
				user.last_name = form.cleaned_data['last_name']
				user.email = form.cleaned_data['email']
				password = hashers.make_password(form.cleaned_data['password'])
				user.password = password

				user.is_staff = False
				user.is_active = True
				user.save()
				context = {
					"first_name" : user.first_name,
					"last_name" : user.last_name,
				}
				return redirect("todolist:index")
			else:
				context = {
					"error" : True
				}

	return render(request, "todolist/register.html", context)

def change_password(request):
	is_user_authenticated = False

	user = authenticate(username="johndoe", password="johndoe1")
	print(user)

	if user is not None:
		authenticated_user = User.objects.get(username="johndoe")
		authenticated_user.set_password("johndoe1")
		authenticated_user.save()
		is_user_authenticated = True

	context = {
		"is_user_authenticated" : is_user_authenticated
	}

	return render(request, "todolist/change_password.html", context)

def login_view(request):
	context = {}
	# If this is a POST request, we need to process the form data
	if request.method == "POST":
		form = LoginForm(request.POST)	# Create a form instance and populate it w/ data from the request
		
		# Checks whether data is valid
		# Runs validation routines for all the form fields and returns True & places the form's data in the 'cleaned data' attribute
		if form.is_valid() == False:	
			form = LoginForm()			# Returns a blank login form
		else:
			# Retrieves the information from the form
			username = form.cleaned_data['username']
			password = form.cleaned_data['password']
			user = authenticate(username = username, password = password)
			context = {
				"username" : username,
				"password" : password
			}
			if user is not None:
				login(request, user)
				return redirect("todolist:index")
			else:
				context = {
					"error" : True
				}

	return render(request, 'todolist/login.html', context)

def logout_view(request):
	logout(request)
	return redirect("todolist:index")

# CREATE views
def add_task(request):
	context = {}

	if request.method == 'POST':
		form = AddTaskForm(request.POST)
		if form.is_valid() == False:
			form = AddTaskForm()
		else:

			task_name = form.cleaned_data['task_name']
			description = form.cleaned_data['description']

			duplicates = ToDoItem.objects.filter(task_name=task_name)

			if not duplicates:
				ToDoItem.objects.create(task_name=task_name, description=description, date_created=timezone.now(), user_id=request.user.id)
				return redirect("todolist:index")

			else:
				context = {
					"error" : True
				}
	return render(request, "todolist/add_task.html", context)

def add_event(request):
	context = {}

	if request.method == 'POST':
		form = AddEventForm(request.POST)
		if form.is_valid() == False:
			form = AddEventForm()
		else:

			event_name = form.cleaned_data['event_name']
			description = form.cleaned_data['description']
			date_created = form.cleaned_data['date_created']

			duplicates = Event.objects.filter(event_name=event_name)

			if not duplicates:
				Event.objects.create(event_name=event_name, description=description, date_created=date_created, user_id=request.user.id)
				return redirect("todolist:index")

			else:
				context = {
					"error" : True
				}
	return render(request, "todolist/add_event.html", context)


# UPDATE views
def update_task(request, todoitem_id):
	
	todoitem = ToDoItem.objects.filter(pk=todoitem_id)

	context = {
		"user" : request.user,
		"todoitem_id" : todoitem_id,
		"task_name" : todoitem[0].task_name,
		"description" : todoitem[0].description,
		"status" : todoitem[0].status
	}

	if request.method == 'POST':
		form = UpdateTaskForm(request.POST)
		if form.is_valid() == False:
			form = UpdateTaskForm()
		else:

			task_name = form.cleaned_data['task_name']
			description = form.cleaned_data['description']
			status = form.cleaned_data['status']

			if todoitem:
				todoitem[0].task_name = task_name
				todoitem[0].description = description
				todoitem[0].status = status

				todoitem[0].save()
				return redirect("todolist:index")

			else:
				context = {
					"error" : True
				}

	return render(request, "todolist/update_task.html", context)

def update_profile(request):
	
	context = {}

	if request.method == 'POST':
		user = User.objects.filter(id=request.user.id)
		context = {
			'first_name' : user[0].first_name,
			'last_name' : user[0].last_name,
			'password' : user[0].password
		}
		
		form = UpdateProfileForm(request.POST)
		if form.is_valid() == False:	
			form = UpdateProfileForm()
		else:
			first_name = form.cleaned_data['first_name']
			last_name = form.cleaned_data['last_name']
			password = hashers.make_password(form.cleaned_data['password'])
			
			if user:
				user[0].first_name = first_name
				user[0].last_name = last_name
				user[0].password = password
				user[0].save()
				return redirect("todolist:index")

			else:
				context = {
					"error" : True
				}

	return render(request, "todolist/update_profile.html", context)


# DELETE views
def delete_task(request, todoitem_id):
	todoitem = ToDoItem.objects.filter(pk=todoitem_id).delete()
	return redirect("todolist:index")