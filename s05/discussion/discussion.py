# Python Class Review
# Class Creation

class sampleClass():
	def __init__ (self, year):
		self.year = year

	def show_year(self):
		print(f'The year is: {self.year}')

# myObj = sampleClass(2020)
# myObj.show_year()

# [SECTION 1] Encapsulation in Python
# A mechanism of wrapping attributes and codes acting on
# the methods together as a single unit
# "Data hiding"

# Example 1:

# The prefix underscore (_) is used as a warning for devs that means:
# "Please be careful about this attribute/method, 
# don't use it outside the declared Class."

class Person():
	# Protected attribute "_name"
	def __init__(self):
		self._name = "John Doe"
		self._age = 18

	# Sample setter
	def set_name(self, name):
		self._name = name

	# Sample getter
	def get_name(self):
		print(f'Name of Person: {self._name}')

	def set_age(self, age):
		self._age = age 

	def get_age(self):
		print(f"Age of Person: {self._age}")

# p1 = Person()
# print(p1.name) <- This will generate an attribute error
# p1.get_name()
# p1.set_name('Jane Smith')
# p1.get_name()

# Mini Exercise
# Add another protected attribute called age and create the necessary getter and setter methods

# p1.get_age()
# p1.set_age(40)
# p1.get_age()

# [SECTION 2] Inheritance in Python
# The transfer of characteristics of a parent class to
# child classes are derived from it
# "Parent-Child Relationship"
# To create an inherited class, add the Parent class as
# the parameter of the Child class
# Syntax: class Child_class(Parent_Class)

# Example 1:
class Employee(Person):
	def __init__(self, employeeId):
		# Method super() is used to invoke the immediate
		# Parent class constructor
		super().__init__()
		self._employeeId = employeeId

	# Methods of the Employee class
	def get_employeeId(self):
		print(f'The Employee ID is: {self._employeeId}')

	def set_employeeId(self, employeeId):
		self._employeeId = employeeId

	# Details Method
	def get_details(self):
		print(f"{self._employeeId} belongs to {self._name}")
		# Note: use double quotations if referencing 
		# multiple attributes

# emp1 = Employee("Emp-001")
# emp1.get_details()
# emp1.get_age()		# Testing to see if super() works
# emp1.set_name("Jane Smith")
# emp1.set_age(40)
# emp1.get_details()

# Mini exercise
    # 1. Create a new class called Student that inherits Person with the additional attributes and methods
    # attributes: Student No, Course, Year Level
    # methods:
    #   Create the necessary getters and setters for each attribute
    #   get_detail: prints the output "<Student name> is currently in year <year level> taking up <Course>"

class Student(Person):
	def __init__ (self):
		super().__init__()
		self._studentNum = 0
		self._course = "NaN"
		self._year_lvl = 0

	def get_studentNum(self):
		print(f'The Student Number is: {self._studentNum}')

	def set_studentNum(self, studentNum):
		self._studentNum = studentNum

	def get_course(self):
		print(f'The Student Course is: {self._course}')

	def set_course(self, course):
		self._course = course

	def get_year(self):
		print(f'The Student Year Level is: {self._course}')

	def set_year(self, year_Lvl):
		self._year_lvl = year_Lvl

	def get_detail(self):
		print(f"{self._name} is currently in year {self._year_lvl} taking up {self._course}")

# stud = Student()
# stud.set_year(4)
# stud.set_course("BSCS")
# stud.get_detail()

# [SECTION 3] Polymorphism in Python
# The method inherited from the parent class is not always fit for the 
# child class. Re-implementation/overriding of methods can be done in
# the child class
# There are different methods to use polymorphism in Python

# Method 1: Function & Objects
# A function can be created that can take any object, allowing polymorphism

# Example 1:
class Admin():
	def is_admin(self):
		print(True)

	def user_type(self):
		print('Admin User')

 
class Customer():
	def is_admin(self):
		print(False)

	def user_type(self):
		print("Customer User")

# Define a test function that will take an object called 'obj'

def test_function(obj):
	obj.is_admin()
	obj.user_type()

# Creating object instance of Admin & Customer
user_admin = Admin()
user_customer = Customer()

# test_function(user_admin)
# test_function(user_customer)

# Method 2: Polymorphism w/ Class Methods
# Python uses two different class types in the same way

# Example 1:
class TeamLead():
	def occupation(self):
		print("Team Lead")

	def hasAuth(self):
		print(True)

class TeamMember():
	def occupation(self):
		print("Team Member")

	def hasAuth(self):
		print(False)

tl1 = TeamLead()
tm1 = TeamMember()

# for person in (tl1, tm1):
# 	person.occupation()

# Method 3: Polymorphism w/ Inheritance
# Polymorphism in Python defines methods in Child class that have the
# same name as methods in the Parent class
# "Method Overriding"

# Example 1: 

class Zuitt():
	def tracks(self):
		print("We are currently offering 3 tracks: Developer Career, Pi-Shape Career, Short Courses")

	def num_of_hours(self):
		print("Learn Web Development in 360 hours!")

class DeveloperCareer(Zuitt):
	# Overriding the Parent's num_of_hours() method
	def num_of_hours(self):
		print("Learn the basics of Web Development in 240 hours!")

class PiShapeCareer(Zuitt):
	# Overriding the Parent's num_of_hours() method
	def num_of_hours(self):
		print("Learn skills for no-code app development in 140 hours!")

class ShortCourses(Zuitt):
	# Overriding the Parent's num_of_hours() method
	def num_of_hours(self):
		print("Learn advanced topics in Web Development in 20 hours!")

course1 = DeveloperCareer()
course2 = PiShapeCareer()
course3 = ShortCourses()

course1.num_of_hours()
course2.num_of_hours()
course3.num_of_hours()

# [SECTION 4] Abstraction in Python
# An abstract class can be considered as a blueprint for other classes
# Python does not offer "abstract" classes by default

# A module must be imported to allow for abstract classes
# ABC = Abstract Base Classes
# The import tells the program to get the 'abc' module of Python to be used
from abc import ABC, abstractclassmethod

# Example of an abstract class
class Polygon(ABC): # <- 'ABC' must be a parameter to make an abstract class
	@abstractclassmethod

	# Created an abstract method print_number_of_sides() that needs to be
	# implemented by classes that will inherit the Polygon class
	def print_number_of_sides(self):
		# This denotes that the method doesn't do anything
		pass 

class Triangle(Polygon):

	def __init__(self):
		super().__init__()

	# Since the triangle class inherited the Polygon class, it must now
	# implement the abstract method
	def print_number_of_sides(self):
		print("This polygon has 3 sides.")

class Pentagon(Polygon):

	def __init__(self):
		super().__init__()

	def print_number_of_sides(self):
		print("This polygon has 5 sides.")

shape1 = Triangle()
shape2 = Pentagon()

shape1.print_number_of_sides()
shape2.print_number_of_sides()


