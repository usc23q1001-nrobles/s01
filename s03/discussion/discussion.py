# User Inputs
# Input Method -> input()
# Allows us to gather data from the user input
# Returns a string datatype

# Example Code 1:
# username = input("Please enter username: \n")
# print(f"Hi {username}! Welcome to Python Short Course")


# How to get a number/int/float input
# num1 = input("Enter 1st number: \n")
# num2 = input("Enter 2nd number: \n")
# print(f"The sum of num1 and num2 is {num1 + num2}")

# The code above will concatenate the two numbers

# Example Code 2:
# num1 = int(input("Enter 1st number: \n"))
# num2 = int(input("Enter 2nd number: \n"))
# print(f"The sum of {num1} and {num2} is {num1 + num2}")
# Use typecasting to do add two user-defined numbers

# If-Else statements
# If-Else are used to choose between two or more
# Code blocks depending on the condition

# Declare a variable to use for the conditional statement
# test_num = 75

# if test_num >= 60:
# 	print("Test Passed")
# else:
# 	print("Test Failed")

# Else-if chains
# test_num2 = int(input("Please enter a number: \n"))

# if test_num2 > 0:
# 	print("The number is positive")
# elif test_num2 == 0:
# 	print("The number is zero")
# else:
# 	print("The number is negative")

# Mini-Exercise:
# Create an if-else statement that determines
# If a number is divisible by 3, by 5, or by both
# if the number is divisible by 3, 
# print "The number is divisible by 3"

# If the number is divisible by 5,
# print "The number is divisible by 5"

# If the number is divisible by both 3 & 5
# print "The number is divisible by both 3 & 5"

# If the number is not divisible by either numbers,
# print "The number is not divisible by 3 nor 5"

# test_num3 = int(input("Please enter a number: \n"))


# if ((test_num3 % 3) == 0) & ((test_num3 % 5) == 0):
# 	print("The number is divisible by both 3 & 5")
# elif (test_num3 % 5) == 0:
# 	print("The number is divisible by 5")
# elif (test_num3 % 3):
# 	print("The number is divisible by 3")
# else:
# 	print ("The number is not divisible by 3 nor 5")

# Loops
# Python has loops that repeat code blocks
# While loops are used to execute a statement
# as long as the condition is true

# Sample Loop Code
# x = 1
# while x <= 5:
# 	print(f"Current count: {x}")
# 	x += 1

# # For Loops - used to iterate a sequence
# fruits = ["apple", "banana", "cherry"]
# for indiv_fruit in fruits:
# 	print(indiv_fruit)

# range()

# for x in range(6, 10, 2):
# 	print(f"The current value is {x}")

# Break Statements 
# Used to stop the loop

# j = 1
# while j < 6:
# 	print(j)
# 	if j == 3:
# 		break
# 	j +=1

# Continue Statement
# Returns to the beginning of the while loop 
# and continue with the next iteration

# k = 1
# while k < 6:
# 	k += 1
# 	if k == 3:
# 		continue
# 	print (k)




