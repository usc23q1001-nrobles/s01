
get_year = input("Please input a year: ")

if get_year.isdigit():
	year = int(get_year)
	if year % 4 == 0:
		print(f"{year} is a leap year\n")
	else:
		print(f"{year} is NOT a leap year\n")
else:
	print(f"{get_year} is not a valid year number\n")



get_row = input("Please input row numbers: ")
get_col = input("Please get column numbers: ")

if get_row.isdigit() and get_col.isdigit():
	row = int(get_row)
	col = int(get_col)
	for x in range(row):
		for y in range(col):
			print("*",end="")
		print()	

