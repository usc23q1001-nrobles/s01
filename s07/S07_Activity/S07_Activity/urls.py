from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('django_practice/', include('django_practice.urls')),
    path('admin/', admin.site.urls),
]