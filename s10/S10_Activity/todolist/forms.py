from django import forms

class LoginForm(forms.Form):
	username = forms.CharField(label="username", max_length="20")
	password = forms.CharField(label="password", max_length="20")

class RegisterForm(forms.Form):
	username = forms.CharField(label="username", max_length="20")
	first_name = forms.CharField(label="first_name", max_length="30")
	last_name = forms.CharField(label="last_name", max_length="30")
	email = forms.EmailField(label="email", max_length="50")
	password = forms.CharField(label="password", max_length="20")

class AddTaskForm(forms.Form):
	task_name = forms.CharField(label="Task Name", max_length="50")
	description = forms.CharField(label="Description", max_length="200")

class UpdateTaskForm(forms.Form):
	task_name = forms.CharField(label="Task Name", max_length="50")
	description = forms.CharField(label="Description", max_length="200")
	status = forms.CharField(label="Status", max_length="50")