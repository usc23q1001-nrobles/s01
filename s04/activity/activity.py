class Camper():
	def __init__(self, name, batch, course_type):
		self.name = name
		self.batch = batch
		self.course_type = course_type

	def print_values(self):
		print(f"Camper Name: {self.name}")
		print(f"Camper Batch: {self.batch}")
		print(f"Camper Course: {self.course_type}")

	def career_track(self):
		print(f"Currently enrolled in the {self.course_type} program.")

	def info(self):
		print(f"My name is {self.name} of Batch {self.batch}.")

zuitt_camper = Camper("Nhem", 17, "Zuitt Python Course")

zuitt_camper.print_values()
zuitt_camper.info()
zuitt_camper.career_track()