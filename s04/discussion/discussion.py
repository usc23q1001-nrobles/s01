# # Python has several structures to store collections
# # or multiple items in a single variable

# # Lists
# # They are similar to arrays in JavaScript in a sense that
# # they can contain a collection of data

# # To create a list, a square bracket "[]" is used.

# # Example 1
# names = ["John", "Paul", "George", "Ringo"]

# # Example 2
# programs = ["developer career", "pi-shape", "short courses"]

# # Example 3
# durations = [260, 180, 20]

# # Example 4
# truth_values = [True, False, True, True, False]

# # Example 5 (Utilizing a List w/ different datatypes)
# sample_list = ["Apple", 3, False, "Potato", 4, True]

# print (sample_list)

# # Getting the list size/number of elements in a list
# print (len(programs))

# # Accessing values in list 
# # List can be accessed by getting the index num of the element

# # Accessing the first list item
# print (programs[0])		#outputs "developer career"

# # Accessing the last list item
# print (programs[-1])	#outputs "short courses"

# # Accessing the second list item
# print (durations[1])	#outputs 180

# # Access the whole list/collection
# print (durations)		#outputs [260, 180, 20]

# # Access a range of values
# # List [start index: end index]
# print (programs[0:2])
# # NOTE: The end index will NOT be included in printing

# # Updating Lists
# # Print the current value
# print (f'Current value: {programs[2]}')

# # Update the value
# programs[2] = 'Short Courses'

# # Print the new value
# print (f'New Value: {programs[2]}')

# # Lists Mini-Exercise
# # 1. Create a List of Names of 5 students
# # 2. Create a List of Grades for the 5 students
# # 3. Use a loop to iterate through the lists 
# # 	 printing in the following format:
# # 	 "The grade of student1 is grades1"

# stud_names = ["John", "Adam", "George", "Abe", "Jodi"]
# stud_grades = [95, 87, 92, 78, 80]

# for x in range(0,4):
# 	print (f'The grade of {stud_names[x]} is {stud_grades[x]}')

# # List Manipulation
# # List has methods used to manipulate the elements within

# # Adding List Items: append() method
# # Allows to insert items to a list
# programs.append("global")
# print (programs)

# # Deleting List Items: "del" keyword
# # Used to delete elements in the list

# # Adding new item
# durations.append(69)
# print (durations)

# # Delete last item on the list
# del durations[-1]
# print (durations)

# # Membership Checks: "in" keyword
# # Checks if the element is in the List
# # Returns a Boolean value
# print (20 in durations)
# print (420 in durations)

# # Sorting Lists: sort() method
# # Sorts the list alphanumerically
# # In ascending order by default
# print (names)
# names.sort()
# print (names)

# # Emptying Lists: clear() method
# # Used to empty the list contents

# test_list = [1,2,3,4]
# print (test_list)
# test_list.clear()
# print (test_list)

# # -----------------------------------------------------

# # Dictionaries
# # Used to store data values in key:value pairs

# # To create a dictionary, curly braces "{}" are used
# # Key-value pairs are denoted w/ (key : value) syntax

# # Example 1
# person1 = {
# 	"name" : "Nikolai",
# 	"age" : 18,
# 	"occupation" : "Student",
# 	"isEnrolled" : True,
# 	"subjects" : ["Python", "SQL", "Django"]
# }

# print (person1)

# # Checking the number of key-value pairs: len()
# print (len(person1))

# # Accessing values in the dictionary
# print (person1["name"])		# outputs "Nikolai"

# # The keys() method will return a List of all keys in a dictionary
# print (person1.keys())

# # The values() method will return a List of all values in a dictionary
# print (person1.values())

# # The items() methods will return each item in a dictionary 
# # as a key-value pair

# print (person1.items())

# # Adding a new key-value pair can be done either by: 
# #    a. putting a new index key & assigning a value, or
# # 	 b. the update() method

# # Option A
# person1["nationality"] = "Filpino"

# # Option B
# person1.update({"fave_food" : "Puto"})

# print (person1)

# # Deleting entries can be done either by:
# # 	a. using the pop() method, or
# # 	b. the del keyword

# # Option A
# person1.pop("fave_food")

# # Option B
# del person1["nationality"]

# print (person1)

# # clear() method empties the dictionary
# person2 = {
# 	"name" : "John Doe",
# 	"age" : 21
# }

# print (person2)
# person2.clear()
# print (person2)

# # Loops in Dictionaries

# for key in person1:
# 	print(f"The value of {key} is {person1[key]}")

# # Nested Dictionaries
# person3 = {
# 	"name" : "Monika",
# 	"age" : 20,
# 	"occupation" : "writer",
# 	"isEnrolled" : True,
# 	"subjects" : ["Python", "SQL", "Django"]
# }

# classRoom = {
# 	"student1" : person1,
# 	"student2" : person2,
# 	"student3" : person3
# }

# # Dictionaries Mini-Exercise
# # 1. Create a car dictionary with the following keys:
# # brand, model, year of make, colour
# # 2. Print the following statements from the details:
# # "I own a <brand> <model> and it was made in <year of make>"

# my_car = {
# 	"brand" : "Lamborghini",
# 	"model" : "Aventador",
# 	"year_of_make": "2020",
# 	"colour": "Matte Black"
# }
# print(f"I own a {my_car['brand']} {my_car['model']} and it was made in {my_car['year_of_make']}") 

# ---------------------------------------------------------

# Functions

# Functions are blocks of code that runs when it is called
# The "def" keyword is used to create a function
# Syntax: def <function_name>()

# Defining a function called my_greeting()
def my_greeting():
	# Code to be run when my_greeting is called back
	print("Hello there, wonderful User!")

# Calling/invoking a defined function
my_greeting()

# Parameters can be added to functions to have more control
# to what the inputs for the function would be

def greet_user(user):
	# prints the value of the user parameter
	print(f"Well hello, Mr./Mrs. {user}!")

# Arguments - values that are substituted to the parameter
greet_user("Anderson")
greet_user("Robinson")

# From a function's perspective:
# 	A parameter is a variable listed inside the parentheses
# 	in the function definition.

# 	Arguments are values sent to the function when it is called

# "return" keyword
# Allow functions to return a value

def addition(num1, num2):
	return num1 + num2

sum = addition(1, 2)

print (f"The sum is {sum}.")

# ---------------------------------------------------------

# Lambda Function
# A small, anonymous function that can be used for callbacks

# Example 1
greeting = lambda person : f'hello {person}'

print (greeting("Elsie"))
print (greeting("Anthony"))

# Sample lambda function w/ two parameters
multiply = lambda a, b : a * b 
print (multiply(5, 6))
print (multiply(6, 99))

# ---------------------------------------------------------

# Classes
# They serve as blueprints to describe the concepts of objects
# The "class" keyword is used to create classes, along with
# the class name that starts w/ an Uppercase character
# Syntax: class ClassName(): 

class Car():
	# Properties
	def __init__(self, brand, model, year_of_make):
		self.brand = brand
		self.model = model
		self.year_of_make = year_of_make

		# Sample of hard-coded class properties
		self.fuel = "Gasoline"
		self.fuel_level = 0

	# Methods
	def fill_fuel(self):
		print(f"Current fuel level: {self.fuel_level}")
		print("Filling up the fuel tank ...")
		self.fuel_level = 100
		print(f"New fuel level: {self.fuel_level}")

	def drive(self, distance):
		print(f"The car is driven {distance} kilometers.")
		print(f"The fuel level left: {self.fuel_level - distance}")

# Creating a new Instance is done by calling the class
# and providing the arguments

new_car = Car("Toyota", "Vios", "2019")

# Displaying a class attributes can be done using 
# the "." notation

print (f"My car is a {new_car.brand} {new_car.model}")

# Calling methods of an Instance
new_car.fill_fuel()
new_car.drive(50)

