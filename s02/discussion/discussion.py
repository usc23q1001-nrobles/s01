# This is a Python comment (yay)
# You can type Ctrl + / to write a Python comment
# Comments in Python start w/ the #
#  #This is awesome

"""

This is a multi-line comment (workaround)
in Python. While there are no keybinds,
you can start and end a multi-line
comment with 3 double quotation marks

"""

# Python Syntax
# Hello World in Python
print ("Hello World!")

# Indentation
# In other languages where indentation is
# for readability, Python indentation is 
# VERY IMPORTANT.

# Variables
# Declaring Variables
# Vars w/ two or more words should be in
# snake_casing (just like this)

age = 23
middle_initial = "B"
name1, name2, name3, name4 = "John", "Paul", "George", "Ringo"

# Data Types in Python
# Strings (str) - for alphanum & symbols

full_name = "John Doe"
secret_code = "P@ssw0rd"

# Numbers (Int, float, complex)

days = 365    			# This is an integer
pi_approx = 3.1416		# This is a float
complex_num = 1 + 5j	# This is a complex-num

# Boolean (bool) - Truth Values

isLearning = True
isDifficult = False

# Using Variables

print("My name is " + full_name)
# print("My age is " + age) <- won't work, age is int not string

# Typecasting
# int() - converts value to int
# float() - converts value to float
# str() - converts value to string

print("My age is " + str(age))

print(int(3.5))

print(int("6969"))

# F-strings

print(f"My age is + {age}")

# Operations
# Arithmetic Operators - math operations

print(1 + 10)		# Addition
print(70 - 1)		# Subtraction
print(9 * 8)		# Multiplication
print(70 / 7)		# Division
print(18 % 5)		# Modulo
print(2 ** 6)		# Exponent

# Assignment Operators
num1 = 3
num1 += 4 			# Addition Assignment
print(num1)

num1 -= 4 			# Subtraction Assignment
print(num1)

num1 *= 4 			# Multiplication Assignment
print(num1)

num1 /= 4 			# Division Assignment
print(num1)

# Comparison Operators - comparing values
# Returns boolean

print (1 == 1)
print (5 > 10)
print (5 < 10)
print (1 <= 1)
print (2 >= 3)
print (1 != 1)

# Logical Operators - returns boolean
print(True and False)
print(True or False)
print(not False)



