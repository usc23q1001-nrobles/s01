from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),

    # Samp route: localhost:8000/todolist/<todoitem_id>
    # localhost:8000/todolist/1
    path('<int:groceryitem_id>/', views.groceryitem, name='viewgroceryitem'),

    # /todolist/register
    path('register', views.register, name='register'),

    # /todolist/change_password
    path('change_password', views.change_password, name='change_password'),

    # /todolist/login
    path('login', views.login_view, name='login'),

    # /todolist/logout
    path('logout', views.logout_view, name='logout'),
]
